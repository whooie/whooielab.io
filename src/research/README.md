# Research

My undergraduate research at Rice was in refining the long-range aspects of the
ground-state strontium molecular potential via the development of numerical
tools to solve the Schrödinger equation. Going forward, my interests lie in the
implementation of quantum algorithms on scalable systems -- notably arrays of
neutral ytterbium-171 Rydberg atoms. Lately I have also developed an interest in
programming language design, particularly how one may design a robust language
to describe quantum computations.

## Pre-prints
- L. Li, X. Hu, Z. Jia, **W. Huie**, W. K. C. Sun, Aakash, Y. Dong, N.
  Hiri-O-Tuppa, J. P. Covey, "Parallelized telecom quantum networking with a
  ytterbium-171 atom array." [arXiv:2502.17406][telecom-arxiv].

[telecom-arxiv]: https://arxiv.org/abs/2502.17406

## Peer-reviewed publications
- Z. Jia, **W. Huie**, L. Li, W. K. C. Sun, X. Hu, Aakash, H. Kogan, A. Karve,
  J. Y. Lee, and J. P. Covey, "An architecture for two-qubit encoding in neutral
  ytterbium-171 atoms." [npj Quantum Information **10**, 106 (2024)][ququart].
  \[[arXiv][ququart-arxiv]\]

[ququart]: https://www.nature.com/articles/s41534-024-00898-7
[ququart-arxiv]: https://arxiv.org/abs/2402.13134

- **W. Huie**, L. Li, N. Chen, X. Hu, Z. Jia, W. K. C. Sun, and J. P. Covey,
  "Repetitive Readout and Real-Time Control of Nuclear Spin Qubits in
  <sup>171</sup>Yb Atoms." [Phys. Rev. X Quantum **4**, 030337
  (2023)][nd-readout].
  \[[arXiv][nd-readout-arxiv]\]

[nd-readout]: https://journals.aps.org/prxquantum/abstract/10.1103/PRXQuantum.4.030337
[nd-readout-arxiv]: https://arxiv.org/abs/2305.02926

- L. Li, **W. Huie**, N. Chen, B. DeMarco, and J. P. Covey, "Active Cancellation
  of Servo-Induced Noise on Stabilized Lasers via Feedforward." [Phys. Rev.
  Applied **18**, 064005 (2022)][servo-noise].
  \[[arXiv][servo-noise-arxiv]\]

[servo-noise]: https://journals.aps.org/prapplied/abstract/10.1103/PhysRevApplied.18.064005
[servo-noise-arxiv]: https://arxiv.org/abs/2208.01012

- N. Chen<sup>\*</sup>, L. Li<sup>\*</sup>, **W. Huie**<sup>\*</sup>, M. Zhao,
  I. Vetter, C. Greene, and J. P. Covey, "Analyzing the Rydberg-based
  optical-metastable-ground architecture for <sup>171</sup>Yb nuclear spins."
  [Phys. Rev. A **105**, 052438 (2022)][yb-omg].
  \[[arXiv][yb-omg-arxiv]\]

[yb-omg]: https://journals.aps.org/pra/abstract/10.1103/PhysRevA.105.052438
[yb-omg-arxiv]: https://arxiv.org/abs/2011.11936

- **W. Huie**, S. G. Menon, H. Bernien, and J. P. Covey, "Multiplexed
  telecommunication-band quantum networking with atom arrays in optical
  cavities." [Phys. Rev. Research **3**, 043154 (2021)][yb-telecom].
  \[[arXiv][yb-telecom-arxiv]\]

[yb-telecom]: https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.3.043154
[yb-telecom-arxiv]: https://arxiv.org/abs/2107.04477

- J. C. Hill, **W. Huie**, P. Lunia, J. D. Whalen, S. K. Kanungo, Y. Lu, and T.
  C. Killian, "Photoassociative Spectroscopy of <sup>87</sup>Sr." [Phys. Rev. A
  **103**, 023111 (2021)][87sr-photoassociation].
  \[[arXiv][87sr-photoassociation-arxiv]\]

[87sr-photoassociation]: https://journals.aps.org/pra/abstract/10.1103/PhysRevA.103.023111
[87sr-photoassociation-arxiv]: https://arxiv.org/abs/2201.04083

- A. Erwin, K. J. Stone, D. Shelton, I. Hahn, **W. Huie**, L. A. N. de Paula, N.
  D. Schmerr, H. J. Paik, and T. C. P. Chui, "Electrostatic frequency reduction:
  A negative stiffness mechanism for measuring dissipation in a mechanical
  oscillator at low frequency." [Rev. Sci. Instrum. **92**, 015101 (2021)][efr].

[efr]: https://aip.scitation.org/doi/10.1063/5.0019351

