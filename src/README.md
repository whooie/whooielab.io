# About me

<div style="text-align: center; width: 80%; margin: auto auto;">
<img src="assets/will_huie.jpg" alt="will_huie"></img>
</div>

Hi! I'm a physics PhD student working in the [Covey Lab][covey-lab] at the
University of Illinois Urbana-Champaign building a programmable tweezer array of
ytterbium-171 atoms. Our platform has myriad potential applications in quantum
computing, networking, and simulation.

[covey-lab]: https://sites.google.com/view/coveylab/

## Links:
- [Email](mailto:whuie2@illinois.edu)
- [Gitlab](https://gitlab.com/users/whooie/projects)
- [Google Scholar](https://scholar.google.com/citations?user=F3tVmEYAAAAJ&hl=en&authuser=1)
- [Blog](https://whooie.gitlab.io/writers-bloch)
- [Resume/CV](https://whooie.gitlab.io/resume)

