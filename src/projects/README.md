# Projects

## [ZX-Calc](https://gitlab.com/whooie/zx-calc)

Extensible tools for working in the ZX-calculus, a diagrammatic language for
reasoning about linear maps between *n*-partite combinations of two-level
quantum states. This project is still in its early stages, and will contain
multiple implementations of the calculus along with interfaces to commonly used
quantum circuit descriptors like OpenQASM.

## [Tensor-Net](https://gitlab.com/whooie/tensor-net)

Small tensor network and matrix product state library for simulating quantum
circuits of qubits with projective measurements and nearest-neighbor couplings.
States are represented in canonical form, allowing efficient calculation of
entanglement entropy. Abstractions are oriented around the study of
measurement-induced phase transitions in Haar-random circuits.

## [Clifford-Sim](https://gitlab.com/whooie/clifford-sim)

Efficient simulator for stabilizer/Clifford circuits in the Gottesman-Knill
tableau representation. Features random sampling of the *N*-qubit Clifford
group, configurable gate sets, and simulated adaptive feedback based on
randomized projective measurements.

## [Multilevel-Sim](https://gitlab.com/whooie/multilevel-sim)

Tools for simulating driven multilevel dynamics in atom-light systems using a
variety of numerical methods for solution of the Schrödinger, Liouville, and
Lindblad equations. Leverages Rust's polymorphic parametricity and traits to
provide type-backed, high-level descriptions of various quantum systems.

## [xspace](https://gitlab.com/whooie/xspace)

Implementations of various numerical methods to solve the time-(in)dependent
Schrödinger equation in arbitrary 1D potentials.

## [QuACS](https://gitlab.com/whooie/quacs/-/tree/rustlib)

**Qu**antum **A**rithmetic and **C**ircuit **S**imulator. Kind of like a baby
version of [Qiskit][qiskit]. Implements hashmap- and array-backed Dirac state
vectors and operators. Provides a library of common gates which can be assembled
into a naive circuit implementation with compilation to the standard
Hadamard/Phase/Controlled-NOT universal gate set. Includes randomized state
readout and mid-circuit projective measurement. Will also include tooling for
implementations of both pure-quantum and hybrid algorithms, as well as
simulation of per-gate noise.

[qiskit]: https://qiskit.org/

## [QLisp](https://gitlab.com/whooie/qlisp)

A small toy lisp written in 100% Rust with familiar Rust-like built-ins. Planned
for eventual integration with QuACS as a high-level DSL for driving the major
functions of the library.

## [SrSolve](https://gitlab.com/whooie/srsolve/-/tree/rustlib)

Implementation of various numerical methods to solve the time-independent
Schrödinger equation. Originally designed for the calculation of bound states of
strontium dimers as part of my senior thesis project, but methods are easily
generalizable to other potentials as well.

